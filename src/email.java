
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import utility.getConnection;
import utility.penangananKomponen;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author iweks
 */
public class email {

    Connection koneksi;
    int ulang = 0;
    static Transport transport;
    static Session session;
    static javax.mail.Message message;
    static String host = "smtp.gmail.com";
    int port = 587;
    cekTabelSmsKeluar keluar;
    static String username = "iweks24@gmail.com";
    static String password = "ternate24";
    String SMSUrl = "http://128.199.232.241/sms/smsmasking.php?username=<username>&key=<sandi>&number=<nohp>&message=<pesan>";
    String SMSUsername = "iweks24";
    String SMSSandi = "6769dc0d45205a63f1af7cdaff0df4a9";
    getConnection u;
    penangananKomponen kom;

    public email() {
        kom = new penangananKomponen();
        u = new getConnection();
        koneksi = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        getKonfigurasi();
        if (!koneksi()) {
            tulis("GAGAL");
        } else {
            keluar = new cekTabelSmsKeluar();
            if (!keluar.isAlive()) {
                //    System.out.println("Ini dia AAA");
                keluar.run();
            }
        }
    }

    int asuku = 0;

    private void getKonfigurasi() {

        String sql = "SELECT host, port, username, sandi FROM musik_master_conf_email WHERE nama = 'EMAIL'";
        Object[] email = kom.setDataEdit(koneksi, sql);
        host = email[0].toString();
        port = Integer.parseInt(email[1].toString());
        username = email[2].toString();
        password = email[3].toString();

        String sql2 = "SELECT host, username, sandi FROM musik_master_conf_email WHERE nama = 'SMS'";
        Object[] sms = kom.setDataEdit(koneksi, sql2);
        SMSUrl = sms[0].toString();
        SMSUsername = sms[1].toString();
        SMSSandi = sms[2].toString();
    }

    private void periksaDataSmsKeluar() {
        boolean ada = false;

        String sql = "SELECT id, tujuan, pesan, gagal FROM musik_pesan WHERE status = 'BELUM' AND methode = 'SMS' AND length(tujuan) > 5";
        //   System.out.println("SQL Ambil KIRIM : " + sql);
        if (this.koneksi == null) {
            this.koneksi = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        }
        try {

            PreparedStatement stat = this.koneksi.prepareStatement(sql);
            ResultSet rSet = stat.executeQuery();
            if (rSet.next()) {
                int id = rSet.getInt(1);
                String tujuan = rSet.getString(2);
                String pesan = rSet.getString(3);
                int gagal = rSet.getInt(4);
                //    String judul = rSet.getString(5);
                String no = String.valueOf(id);

                gagal += 1;

                boolean update = insertData(no, "PROSES");
                if (update) {
                    String nama = "";
                    tulis("--------------------");
                    tulis("Tujuan : " + tujuan);
                    tulis("Waktu  : " + new getWaktu().getWaktu());
                    //       tulis("Gagal  : " + String.valueOf(gagal - 1));
                    //        tulis("\"" + pesan + "\"");
                    kirim_SMS(no, tujuan, pesan);
                }

                ada = true;
            }
            stat.close();
        } catch (SQLException ex) {
            tulis("Koneksi Database Putus");
            tulis("----------------------");
            koneksi = u.getConnection(u.jdbc, u.url, u.user, u.pass);
            ex.printStackTrace();
            asuku++;
            if (asuku % 5 == 0) {

                //         kirim_manual("giri@ikin.co.id", "HTMLYang Mulia Bapak Raden Giri Subekti<br />Koneksi Database BBN Terputus yang mengakibatkan email gateway tidak berjalan dengan baik. mohon di remote", "Koneksi DB Arista Putus");
                //         kirim_manual("anto@ikin.co.id", "HTMLYang Mulia Bapak Raden Giri Subekti<br />Koneksi Database BBN Terputus yang mengakibatkan email gateway tidak berjalan dengan baik. mohon di remote", "Koneksi DB Arista Putus");
                //          kirim_manual("adi@ikin.co.id", "HTMLYang Mulia Bapak Raden Giri Subekti<br />Koneksi Database BBN Terputus yang mengakibatkan email gateway tidak berjalan dengan baik. mohon di remote", "Koneksi DB Arista Putus");
            }
        }

        //   if (!ada) {
        //  periksaDataSmsKeluarNotifikasi();
        //   }
    }

    private void kirim_SMS(String id_sms, String ke, String pesan) {

        try {
            String g = SMSUrl;
            g = g.replaceAll("<username>", SMSUsername);
            g = g.replaceAll("<sandi>", SMSSandi);
            g = g.replaceAll("<nohp>", ke);
            g = g.replaceAll("<pesan>", URLEncoder.encode(pesan, "UTF-8"));
            //      out.println("1. " + g + "\n");
            URL u = new URL(g);
            URLConnection c = u.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    c.getInputStream()));
            //     out.println("3. " + g + "\n");
            String inputLine;
            StringBuffer b = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.startsWith("0")) {
                    tulis("Sukses Kirim SMS");
                    insertData(id_sms, "SUKSES");
                } else {
                    tulis("Gagal Kirim SMS - " + inputLine);
                    insertData(id_sms, "GAGAL");
                }

            }
            in.close();

        } catch (MalformedURLException ex) {
            tulis("Gagal Kirim SMS - " + ex.getMessage());
            insertData(id_sms, "GAGAL");
        } catch (IOException e) {
            tulis("Gagal Kirim SMS - " + e.getMessage());
            insertData(id_sms, "GAGAL");
        }
    }

    private void periksaDataEmailKeluar() {
        boolean ada = false;

        String sql = "SELECT id, tujuan, pesan, gagal, judul FROM musik_pesan WHERE status = 'BELUM' AND methode = 'EMAIL'  AND length(tujuan) > 5";
        //   System.out.println("SQL Ambil KIRIM : " + sql);
        if (this.koneksi == null) {
            this.koneksi = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        }
        try {

            PreparedStatement stat = this.koneksi.prepareStatement(sql);
            ResultSet rSet = stat.executeQuery();
            if (rSet.next()) {
                int id = rSet.getInt(1);
                String tujuan = rSet.getString(2);
                String pesan = rSet.getString(3);
                int gagal = rSet.getInt(4);
                String judul = rSet.getString(5);
                String no = String.valueOf(id);

                gagal += 1;

                boolean update = insertData(no, "PROSES");
                if (update) {
                    String nama = "";
                    tulis("--------------------");
                    tulis("Tujuan : " + tujuan);
                    tulis("Waktu  : " + new getWaktu().getWaktu());
                    //        tulis("Gagal  : " + String.valueOf(gagal - 1));
                    //         tulis("\"" + pesan + "\"");
                    kirim_YM(no, tujuan, pesan, judul);
                }

                ada = true;
            }
            stat.close();
        } catch (SQLException ex) {
            tulis("Koneksi Database Putus");
            tulis("----------------------");
            koneksi = u.getConnection(u.jdbc, u.url, u.user, u.pass);
            ex.printStackTrace();
            asuku++;
            if (asuku % 5 == 0) {

                //         kirim_manual("giri@ikin.co.id", "HTMLYang Mulia Bapak Raden Giri Subekti<br />Koneksi Database BBN Terputus yang mengakibatkan email gateway tidak berjalan dengan baik. mohon di remote", "Koneksi DB Arista Putus");
                //         kirim_manual("anto@ikin.co.id", "HTMLYang Mulia Bapak Raden Giri Subekti<br />Koneksi Database BBN Terputus yang mengakibatkan email gateway tidak berjalan dengan baik. mohon di remote", "Koneksi DB Arista Putus");
                //          kirim_manual("adi@ikin.co.id", "HTMLYang Mulia Bapak Raden Giri Subekti<br />Koneksi Database BBN Terputus yang mengakibatkan email gateway tidak berjalan dengan baik. mohon di remote", "Koneksi DB Arista Putus");
            }
        }

        //   if (!ada) {
        //  periksaDataSmsKeluarNotifikasi();
        //   }
    }

    private void kirim_YM(String id_sms, String ke, String pesan, String judul) {
        boolean ok = false;
        boolean update;
        try {
            pesan = "HTML" + pesan.replaceFirst("HTML", "");
            message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(ke));

            message.setSubject(judul);
            if (pesan.startsWith("HTML")) {
                ok = true;
                pesan = pesan.replaceFirst("HTML", "");
                message.setContent(pesan, "text/html; charset=ISO-8859-1");
            } else {
                message.setText(pesan);
            }

            transport.sendMessage(message, message.getAllRecipients());
            tulis("Sukses Kirim Email");
            update = insertData(id_sms, "SUKSES");
        } catch (MessagingException e) {

            if (ok) {
                pesan = "HTML" + pesan.replaceFirst("HTML", "");
            }
            try {
                koneksi();
                message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(ke));

                message.setSubject(judul);
                if (pesan.startsWith("HTML")) {
                    pesan = pesan.replaceFirst("HTML", "");
                    message.setContent(pesan, "text/html; charset=ISO-8859-1");
                } else {
                    message.setText(pesan);
                }

                transport.sendMessage(message, message.getAllRecipients());
                tulis("Sukses Kirim Email Ulang");
                insertData(id_sms, "SUKSES");

                //     System.out.println("Done 22");
            } catch (MessagingException ex) {
                update = insertData(id_sms, "GAGAL");
            }
        }
        try {
            cekTabelSmsKeluar.sleep(4000L);
        } catch (InterruptedException ex) {
            tulis(ex.getMessage());
        }
    }

    private boolean insertData(String id, String aksi) {
        boolean ok = false;
        String sql = "UPDATE musik_pesan SET status = '" + aksi + "' WHERE id = " + id;
        ok = kom.setSQL(koneksi, sql);
        return ok;
    }

    public boolean koneksi() {
        boolean ok = false;
        this.ulang += 1;
        try {
            System.out.println("Mencoba terhubung dengan SMTP .....");
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.debug", "true");
   //         props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.socketFactory.port", port);
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.socketFactory.fallback", "false");

            //   host = this.u.host;
            //  this.port = Integer.parseInt(port);
            session = Session.getInstance(props);
            transport = session.getTransport("smtp");

            transport.connect(host, port, username, password);
            System.out.println("Sukses Terhubung dengan Akun " + username);
            ok = true;
        } catch (MessagingException ex) {
            System.out.println(ex.getMessage());
            ok = false;
        }

        return ok;
    }

    private void tulis(String proses) {
        System.out.println(proses);
        String tanggal = new getWaktu().getTanggal();
        String bulan = new getWaktu().getBulan();
        String thn = new getWaktu().getTahun();
        String jam = new getWaktu().getJam();
        String menit = new getWaktu().getMenit();
        String detik = new getWaktu().getDetik();
        String waktu = jam + ":" + menit + ":" + detik;
        String ddmmyy = tanggal + bulan + thn;
        String tulis_waktu = new getWaktu().getWaktu();

        proses = "** " + tulis_waktu + " **      " + proses + "\n";
        try {
            byte b[] = proses.getBytes();
            java.io.File outputx = new java.io.File("LOG/" + ddmmyy + ".log");
            java.io.RandomAccessFile random = new java.io.RandomAccessFile(outputx, "rw");

            random.seek(outputx.length());
            random.write(b);
            random.close();
        } catch (java.io.IOException ex) {
            System.out.println(ex);
        }

    }

    public static void main(String args[]) {
        new email();
    }

    private class cekTabelSmsKeluar extends Thread {

        /**
         * Creates a new instance of TreadSmsKeluar
         */
        public cekTabelSmsKeluar() {
            this.setDaemon(true);
        }

        public void run() {
            //   System.out.println("Ini dia");
            while (true) {
                //       System.out.println("Ini dia");
                periksaDataSmsKeluar();
                periksaDataEmailKeluar();
                //     periksaDataTunda();
                try {
                    Thread.sleep(2001);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
